# go-sqlite3を使ったデータの挿入、読み込み、更新、削除

## データベースの準備

`schema.sql` を実行してテーブルを生成する。

```
$ sqlite3 crud.db < schema.sql
```

参考サイト:

* SQLiteデータベースの使用 | build-web-application-with-golang : https://astaxie.gitbooks.io/build-web-application-with-golang/content/ja/05.3.html
