package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	// SQLite3のデータベースファイルを開く
	// dbはsql.DBのオブジェクト
	db, err := sql.Open("sqlite3", "./crud.db")
	if err != nil {
		log.Fatal(err)
	}
	defer func() { db.Close() }()

	// データを挿入する
	stmt, err := db.Prepare("INSERT INTO greeting(message) VALUES(?)") // パラメータを使ったsqlステートメントを作成する
	if err != nil {
		log.Fatal(err)
	}

	res, err := stmt.Exec("Hello, world") // sqlステートメントのパラメータを設定して実行する
	if err != nil {
		log.Fatal(err)
	}

	id, err := res.LastInsertId()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("row id:", id)

	// データを更新する
	stmt, err = db.Prepare("UPDATE greeting SET message=? WHERE id=?")
	if err != nil {
		log.Fatal(err)
	}
	res, err = stmt.Exec("こんにちは、世界", id)
	if err != nil {
		log.Fatal(err)
	}

	affect, err := res.RowsAffected()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("affected rows:", affect)

	// データの検索
	rows, err := db.Query("SELECT * FROM greeting")
	if err != nil {
		log.Fatal(err)
	}
	for rows.Next() {
		var id int
		var message string
		err = rows.Scan(&id, &message)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("id:", id, "message:", message)
	}

	// データの削除
	stmt, err = db.Prepare("DELETE FROM greeting WHERE id=?")
	if err != nil {
		log.Fatal(err)
	}

	res, err = stmt.Exec(id)
	if err != nil {
		log.Fatal(err)
	}

	affect, err = res.RowsAffected()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("affected rows:", affect)
}
